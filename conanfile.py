from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import subprocess


class wxPdfDocumentConan(ConanFile):
    name = "wxpdfdocument"
    description = "wxpdfdocument"
    topics = ("conan", "wxpdfdocument")
    url = "https://gitlab.com/terranum-conan/wxpdfdocument"
    homepage = "https://github.com/utelle/wxpdfdoc"
    license = "wxWidgets"
    generators = ["cmake", "cmake_find_package"]
    settings = "os", "arch", "compiler", "build_type"
    _cmake = None
    _source_directory = "wxpdfdoc-1.0.2"
    version = "1.0.3-beta"
    options = {
        "shared": [True, False],
        "wx_version": ["3.1.4", "3.1.6", "3.2.1", "3.2.2.1", "3.2.3", "3.2.4", "3.2.5", "3.2.6"],
    }
    default_options = {
        "shared": False,
        "wx_version": "3.2.6",
    }

    def configure(self):
        #if self.settings.os == "Linux" and self.options.wx_version != "3.2.1":
        #    self.options["wxwidgets"].webview = False # webview control isn't available on linux for version before 3.2.1.
        if self.settings.os == "Linux" and self.options.wx_version == "3.2.3":
            self.options["wxwidgets"].png = "system" # PNG system library is needed to avoid crash when displaying menu icons.
        if self.settings.os == "Linux" and self.options.wx_version == "3.2.4":
            self.options["wxwidgets"].png = "system" # PNG system library is needed to avoid crash when displaying menu icons.
        if self.settings.os == "Linux" and self.options.wx_version == "3.2.5":
            self.options["wxwidgets"].png = "system" # PNG system library is needed to avoid crash when displaying menu icons.
        if self.settings.os == "Linux" and self.options.wx_version == "3.2.6":
            self.options["wxwidgets"].png = "system" # PNG system library is needed to avoid crash when displaying menu icons.


    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC
        if self.settings.os != 'Linux':
            self.options.remove('cairo')

    def system_requirements(self):
        if self.settings.os == 'Linux' and tools.os_info.is_linux:
            if tools.os_info.with_apt:
                installer = tools.SystemPackageTool()
                packages = []
                packages.append('libfontconfig1-dev')

    def requirements(self):
        if (self.options.wx_version == "3.1.4"):
            self.requires('wxwidgets/3.1.4@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.1.6"):
            self.requires('wxwidgets/3.1.6@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.1"):
            self.requires('wxwidgets/3.2.1@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.2.1"):
            self.requires('wxwidgets/3.2.2.1@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.3"):
            self.requires('wxwidgets/3.2.3@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.4"):
            self.requires('wxwidgets/3.2.4@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.5"):
            self.requires('wxwidgets/3.2.5@terranum-conan+wxwidgets/stable')
        if (self.options.wx_version == "3.2.6"):
            self.requires('wxwidgets/3.2.6@terranum-conan+wxwidgets/stable')

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], strip_root=True)
        print(self.source_folder)

    def export_sources(self):
        self.copy("CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include/wx", src=os.path.join(self.source_folder, "include/wx"))
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
