#include <cstdlib>
#include <iostream>
#include "wx/pdfdoc.h"
#include "wx/pdffont.h"
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

int main(int argc, char **argv) {
  wxApp::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE, "program");
  wxInitializer initializer;
  if (!initializer) {
    fprintf(stderr, "Failed to initialize the wxWidgets library, aborting.");
    return -1;
  }

  if (argc == 1) {
    wxPrintf("Welcome to the wxPdfDocument\n");
  }

  wxPdfDocument pdf;
  pdf.AddPage(wxPORTRAIT, wxPAPER_A4);
  pdf.SetFont("Helvetica", "B", 16);
  pdf.Cell(40, 10, wxS("Hello World from wxPDFDocument"));
  pdf.SaveAsFile(wxS("tutorial1.pdf"));
  return 0;
}
