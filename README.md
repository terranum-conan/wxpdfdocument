# wxPdfDocument #

## One step package creation

        conan create . terranum-conan+wxpdfdocument/stable

## Upload package to gitlab

      conan upload wxpdfdocument/1.0.3-beta@terranum-conan+wxpdfdocument/stable --remote=gitlab -q 'build_type=Release'

## Build the debug package

      conan create . terranum-conan+wxpdfdocument/stable -s build_type=Debug

Don't upload debug package on Gitlab.

## Step by step package creation

1. Get source code

        conan source . --source-folder=_bin/source
2. Create install files

        conan install . --install-folder=_bin/build
3. Build

        conan build . --source-folder=_bin/source --build-folder=_bin/build
4. Create package 

        conan package . --source-folder=_bin/source --build-folder=_bin/build --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+wxpdfdocument/stable --source-folder=_bin/source --build-folder=_bin/build
6. Test package
      
        conan test test_package wxpdfdocument/1.0.3-beta@terranum-conan+wxpdfdocument/stable

## supported options

        -o wx_version=3.14 or 3.1.6 or 3.2.1

## Linux

For wxPdfdocument to work, the fontconfig library must also be linked under Linux. The following code can be used:

        SET(ADDED_LIBS "")
        if (UNIX)
                SET(ADDED_LIBS "fontconfig")
        endif()

        target_link_libraries(${PROJECT_NAME} ${CONAN_LIBS} ${ADDED_LIBS})



        